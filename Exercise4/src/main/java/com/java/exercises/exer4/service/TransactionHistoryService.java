/**
 * 
 */
package com.java.exercises.exer4.service;

import java.util.List;

import com.java.exercises.exer4.entity.TransactionHistory;

/**
 * @author b.orobia
 *
 */
public interface TransactionHistoryService {
	
	void recordTransactionHistory(TransactionHistory transactionHistory);
	
	List<TransactionHistory> retrieveTransHistoryByUserId(Integer userId);

}
