/**
 * 
 */
package com.java.exercises.exer4.entity;

/**
 * @author b.orobia
 *
 */
public class User {

	private Integer userId;
	private String userName;
	private Long userAmount;

	public User() {

	}

	public User(int userId, String userName, Long userAmount) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userAmount = userAmount;

	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getUserAmount() {
		return userAmount;
	}

	public void setUserAmount(Long userAmount) {
		this.userAmount = userAmount;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", userAmount=" + userAmount + "]";
	}

}
