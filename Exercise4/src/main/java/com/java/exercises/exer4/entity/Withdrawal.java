/**
 * 
 */
package com.java.exercises.exer4.entity;

/**
 * @author b.orobia
 *
 */
public class Withdrawal {

	private Integer userId;
	private Long withdrawAmount;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getWithdrawAmount() {
		return withdrawAmount;
	}

	public void setWithdrawAmount(Long withdrawAmount) {
		this.withdrawAmount = withdrawAmount;
	}



	@Override
	public String toString() {
		return "Withdrawal [userId=" + userId + ", withdrawAmount=" + withdrawAmount;
	}

}
