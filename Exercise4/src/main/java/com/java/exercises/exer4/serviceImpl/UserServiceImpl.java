/**
 * 
 */
package com.java.exercises.exer4.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer4.entity.User;
import com.java.exercises.exer4.mapper.UserMapper;
import com.java.exercises.exer4.service.UserService;

/**
 * @author b.orobia
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper mapper;

	@Override
	public void createUser(User user) {

		if (user.getUserName() == null || user.getUserName().trim().length() == 0) {
			throw new IllegalArgumentException("Missing User Account Name!");
		}

		if (user.getUserAmount() == 0) {
			throw new IllegalArgumentException("No initial deposit amount!");
		}

		try {
			mapper.insertUser(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<User> selectAllUser() {
		List<User> userList = mapper.selectAllUser();
		return userList;
	}

	@Override
	public int countUser() {

		return mapper.countUser();
	}

	@Override
	public void updateUserAmount(Integer userId, Long amount) {

		try {
			
			mapper.updateUserAmount(userId, amount);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
