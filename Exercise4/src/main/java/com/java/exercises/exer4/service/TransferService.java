/**
 * 
 */
package com.java.exercises.exer4.service;

import com.java.exercises.exer4.entity.Transfer;

/**
 * @author b.orobia
 *
 */
public interface TransferService {
	
	void createTransfer(Transfer transfer);

}
