/**
 * 
 */
package com.java.exercises.exer4.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.java.exercises.exer4.entity.User;

/**
 * @author b.orobia
 *
 */
@Mapper
public interface UserMapper {

	@Insert("INSERT INTO mini_bank_user (user_name, user_amount) VALUES (#{userName}, #{userAmount})")
	void insertUser(User user);

	@Select("SELECT * FROM mini_bank_user")
	List<User> selectAllUser();

	@Select("SELECT COUNT(*) FROM mini_bank_user")
	int countUser();

	@Update("UPDATE mini_bank_user SET user_amount = #{userAmount} WHERE user_id = #{userId}")
	void updateUserAmount(Integer userId, Long userAmount);

}
