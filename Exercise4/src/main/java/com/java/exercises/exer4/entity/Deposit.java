package com.java.exercises.exer4.entity;

/**
 * @author b.orobia
 *
 */
public class Deposit {

	private Integer userId;
	private Long depositAmount;

	public Deposit() {

	}

	public Deposit(Integer userId, Long depositAmount) {
		this.userId = userId;
		this.depositAmount = depositAmount;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Long getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(Long depositAmount) {
		this.depositAmount = depositAmount;
	}

	@Override
	public String toString() {
		return "Deposit [userId=" + userId + ", depositAmount=" + depositAmount;
	}

}
