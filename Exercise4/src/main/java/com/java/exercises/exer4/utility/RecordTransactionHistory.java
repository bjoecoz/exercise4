/**
 * 
 */
package com.java.exercises.exer4.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer4.entity.TransactionHistory;
import com.java.exercises.exer4.service.TransactionHistoryService;

/**
 * @author b.orobia
 *
 */
@Service
public class RecordTransactionHistory {

	@Autowired
	private TransactionHistoryService transactionHistoryService;

	public void createLog(Integer userId, Long amount, String type) {
		TransactionHistory deposityHistory = new TransactionHistory();
		deposityHistory.setUserId(userId);
		deposityHistory.setTransactionType(type);
		if (type.equalsIgnoreCase("DEPOSIT")) {
			deposityHistory.setTransactionDetail("Deposit amount of " + amount);
		} else if (type.equalsIgnoreCase("WITHDRAW")) {
			deposityHistory.setTransactionDetail("Withdrawal amount of " + amount);
		} else if (type.equalsIgnoreCase("TRANSFER")) {
			deposityHistory.setTransactionDetail("Transfer amount of " + amount);
		}

		transactionHistoryService.recordTransactionHistory(deposityHistory);
	}

}
