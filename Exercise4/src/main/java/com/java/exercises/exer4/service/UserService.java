/**
 * 
 */
package com.java.exercises.exer4.service;

import java.util.List;

import com.java.exercises.exer4.entity.User;

/**
 * @author b.orobia
 *
 */
public interface UserService {

	void createUser(User user);

	List<User> selectAllUser();

	int countUser();

	void updateUserAmount(Integer userId, Long amount);

}
