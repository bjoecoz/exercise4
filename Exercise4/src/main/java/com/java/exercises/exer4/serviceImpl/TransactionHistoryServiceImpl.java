/**
 * 
 */
package com.java.exercises.exer4.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer4.entity.TransactionHistory;
import com.java.exercises.exer4.mapper.TransactionHistoryMapper;
import com.java.exercises.exer4.service.TransactionHistoryService;

/**
 * @author b.orobia
 *
 */
@Service
public class TransactionHistoryServiceImpl implements TransactionHistoryService {

	@Autowired
	private TransactionHistoryMapper mapper;

	@Override
	public void recordTransactionHistory(TransactionHistory transactionHistory) {

		mapper.recordTransactionHistory(transactionHistory);

	}

	@Override
	public List<TransactionHistory> retrieveTransHistoryByUserId(Integer userId) {

		List<TransactionHistory> transactioHistory = mapper.retrieveTransactionHistory();
		List<TransactionHistory> filteredTransactioHistory = new ArrayList<TransactionHistory>();

		for (TransactionHistory origTransacList : transactioHistory) {
			if (userId == origTransacList.getUserId()) {
				TransactionHistory transacHistoryTemp = new TransactionHistory();
				transacHistoryTemp.setUserId(userId);
				transacHistoryTemp.setTransactionType(origTransacList.getTransactionType());
				transacHistoryTemp.setTransactionDetail(origTransacList.getTransactionDetail());

				filteredTransactioHistory.add(transacHistoryTemp);
			}
		}

		return filteredTransactioHistory;
	}

}
