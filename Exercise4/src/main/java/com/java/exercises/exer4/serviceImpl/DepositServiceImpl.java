/**
 * 
 */
package com.java.exercises.exer4.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer4.entity.Deposit;
import com.java.exercises.exer4.entity.User;
import com.java.exercises.exer4.service.DespositService;
import com.java.exercises.exer4.service.UserService;
import com.java.exercises.exer4.utility.RecordTransactionHistory;

/**
 * @author b.orobia
 *
 */
@Service
public class DepositServiceImpl implements DespositService {

	final static String TRANSACTION_TYPE = "DEPOSIT";

	@Autowired
	private UserService userService;

	@Autowired
	private RecordTransactionHistory recordHistory;

	@Override
	public void createDeposit(Deposit deposit) {

		List<User> userList = userService.selectAllUser();

		for (User user : userList) {
			if (user.getUserId() == deposit.getUserId()) {
				Long newAmount = 0L;
				newAmount = user.getUserAmount() + deposit.getDepositAmount();

				userService.updateUserAmount(user.getUserId(), newAmount);
				recordHistory.createLog(deposit.getUserId(), deposit.getDepositAmount(), TRANSACTION_TYPE);
			}
		}
	}

}
