/**
 * 
 */
package com.java.exercises.exer4.service;

import com.java.exercises.exer4.entity.Withdrawal;

/**
 * @author b.orobia
 *
 */
public interface WithdrawService {
	
	void createWithDraw(Withdrawal withdraw);

}
