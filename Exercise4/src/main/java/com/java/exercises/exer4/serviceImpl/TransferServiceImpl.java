/**
 * 
 */
package com.java.exercises.exer4.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer4.entity.Transfer;
import com.java.exercises.exer4.entity.User;
import com.java.exercises.exer4.service.TransferService;
import com.java.exercises.exer4.service.UserService;
import com.java.exercises.exer4.utility.RecordTransactionHistory;

/**
 * @author b.orobia
 *
 */
@Service
public class TransferServiceImpl implements TransferService {

	final static String TRANSACTION_TYPE = "TRANSFER";

	@Autowired
	private UserService userService;

	@Autowired
	private RecordTransactionHistory recordHistory;

	@Override
	public void createTransfer(Transfer transfer) {

		List<User> userListFrom = userService.selectAllUser();
		List<User> userListTo = userListFrom;
		for (User userFrom : userListFrom) {

			if (transfer.getUserIdFrom() == userFrom.getUserId()) {
				Long newAmountFrom = 0L;
				newAmountFrom = userFrom.getUserAmount() - transfer.getTransferAmount();
				userService.updateUserAmount(transfer.getUserIdFrom(), newAmountFrom);

				for (User userTo : userListTo) {
					if (transfer.getUserIdTo() == userTo.getUserId()) {

						Long newAmountTo = 0L;
						newAmountTo = userTo.getUserAmount() + transfer.getTransferAmount();
						userService.updateUserAmount(transfer.getUserIdTo(), newAmountTo);

					}

				}
				recordHistory.createLog(transfer.getUserIdFrom(), transfer.getTransferAmount(), TRANSACTION_TYPE);
			}

		}

	}

}
