/**
 * 
 */
package com.java.exercises.exer4.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.java.exercises.exer4.entity.Deposit;
import com.java.exercises.exer4.entity.Transfer;
import com.java.exercises.exer4.entity.User;
import com.java.exercises.exer4.entity.Withdrawal;
import com.java.exercises.exer4.service.DespositService;
import com.java.exercises.exer4.service.TransactionHistoryService;
import com.java.exercises.exer4.service.TransferService;
import com.java.exercises.exer4.service.UserService;
import com.java.exercises.exer4.service.WithdrawService;

/**
 * @author b.orobia
 *
 */
@RestController
@RequestMapping("/mini-bank/user")
public class MiniBankController {

	@Autowired
	private UserService userService;

	@Autowired
	private DespositService depositService;

	@Autowired
	private WithdrawService withdrawService;

	@Autowired
	private TransferService transferService;

	@Autowired
	private TransactionHistoryService transactionHistService;

	@PostMapping
	public ResponseEntity<?> addUser(@RequestBody User user) {

		userService.createUser(user);

		return ResponseEntity.ok().build();

	}

	@GetMapping("/all")
	public ResponseEntity<Object> retrieveAllUser() {

		return ResponseEntity.ok(userService.selectAllUser());

	}

	@GetMapping("/all/count")
	public ResponseEntity<Object> countUser() {

		return ResponseEntity.ok(userService.countUser());

	}

	@PostMapping("/deposit")
	public ResponseEntity<?> doDeposit(@RequestBody Deposit deposit) {
		depositService.createDeposit(deposit);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/withdraw")
	public ResponseEntity<?> doWithDraw(@RequestBody Withdrawal withdraw) {
		withdrawService.createWithDraw(withdraw);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/transfer")
	public ResponseEntity<?> doTransfer(@RequestBody Transfer transfer) {
		transferService.createTransfer(transfer);
		return ResponseEntity.ok().build();
	}

	@GetMapping("all/transaction-history")
	public ResponseEntity<?> retrieveAllTransactions(@RequestParam("userId") String userId) {

		return ResponseEntity.ok(transactionHistService.retrieveTransHistoryByUserId(Integer.valueOf(userId)));
	}

}
