/**
 * 
 */
package com.java.exercises.exer4.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.java.exercises.exer4.entity.User;
import com.java.exercises.exer4.entity.Withdrawal;
import com.java.exercises.exer4.service.UserService;
import com.java.exercises.exer4.service.WithdrawService;
import com.java.exercises.exer4.utility.RecordTransactionHistory;

/**
 * @author b.orobia
 *
 */
@Service
public class WithDrawServiceImpl implements WithdrawService {

	final static String TRANSACTION_TYPE = "WITHDRAW";

	@Autowired
	private UserService userService;

	@Autowired
	private RecordTransactionHistory recordHistory;

	@Override
	public void createWithDraw(Withdrawal withdraw) {

		List<User> userList = userService.selectAllUser();

		for (User user : userList) {
			if (user.getUserId() == withdraw.getUserId()) {
				Long newAmount = 0L;

				newAmount = user.getUserAmount() - withdraw.getWithdrawAmount();

				userService.updateUserAmount(user.getUserId(), newAmount);
				recordHistory.createLog(withdraw.getUserId(), withdraw.getWithdrawAmount(), TRANSACTION_TYPE);
			}
		}

	}

}
