/**
 * 
 */
package com.java.exercises.exer4.entity;

/**
 * @author b.orobia
 *
 */
public class Transfer {

	private Integer userIdFrom;
	private Integer userIdTo;
	private Long transferAmount;

	public Integer getUserIdFrom() {
		return userIdFrom;
	}

	public void setUserIdFrom(Integer userIdFrom) {
		this.userIdFrom = userIdFrom;
	}

	public Integer getUserIdTo() {
		return userIdTo;
	}

	public void setUserIdTo(Integer userIdTo) {
		this.userIdTo = userIdTo;
	}

	public Long getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(Long transferAmount) {
		this.transferAmount = transferAmount;
	}

	@Override
	public String toString() {
		return "Transfer [userIdFrom=" + userIdFrom + ", userIdTo=" + userIdTo + ", transferAmount=" + transferAmount
				+ "]";
	}

}
