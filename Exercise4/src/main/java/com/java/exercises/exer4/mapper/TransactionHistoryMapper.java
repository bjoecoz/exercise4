/**
 * 
 */
package com.java.exercises.exer4.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.java.exercises.exer4.entity.TransactionHistory;

/**
 * @author b.orobia
 *
 */
@Mapper
public interface TransactionHistoryMapper {

	@Insert("INSERT INTO mini_bank_transaction_history (user_id, transaction_type, transaction_date, transaction_detail) VALUES (#{userId}, #{transactionType}, NOW(), #{transactionDetail})")
	void recordTransactionHistory(TransactionHistory transactionHistory);
	
	@Select("SELECT * FROM mini_bank_transaction_history")
	List<TransactionHistory> retrieveTransactionHistory();

}
