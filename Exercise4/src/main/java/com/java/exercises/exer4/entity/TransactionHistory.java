/**
 * 
 */
package com.java.exercises.exer4.entity;

/**
 * @author b.orobia
 *
 */
public class TransactionHistory {

	private Integer userId;
	private String transactionType;
	private String transactionDetail;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail(String transactionDetail) {
		this.transactionDetail = transactionDetail;
	}

	@Override
	public String toString() {
		return "TransactionHistory [userId=" + userId + ", transactionType=" + transactionType + ", transactionDetail="
				+ transactionDetail + "]";
	}

}
