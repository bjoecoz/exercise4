CREATE TABLE IF NOT EXISTS mini_bank_user (
 user_id integer not null auto_increment,
 user_name varchar(56),
 user_amount decimal(10,2),
 primary key(user_id)
);

CREATE TABLE IF NOT EXISTS mini_bank_transaction_history (
	user_id integer, 
    transaction_type varchar(26), 
	transaction_detail varchar(200), 
    transaction_date date
);
